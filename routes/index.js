var express = require('express');
var router = express.Router();

var db = require('../queries');


router.get('/api/types', db.getAllTypes);
router.post('/api/types', db.createTypes);
router.put('/api/types', db.updateType);


module.exports = router;