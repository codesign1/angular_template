angular.module('app.controllers', [])

    .controller('homeCrtl', ['$scope', '$state', '$location',
        function ($scope, $state, $location) {
            $scope.isActive = function (viewLocation) {
                return viewLocation === $location.path();
            };
        }
    ])