angular.module('app.directives', [])

    .directive('navbar', [function () {
        return {
            templateUrl: 'templates/navigationbar.html'
        };
    }])
    .directive('foot', [function () {
        return {
            templateUrl: 'templates/footer.html'
        };
    }]);