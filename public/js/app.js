angular.module('app',
    ['ui.router',
        'app.controllers',
        'app.directives',
        'app.services',
        'ui.bootstrap.modal',
        'ui.bootstrap'])