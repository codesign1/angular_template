angular.module('app')

.config(function($stateProvider, $urlRouterProvider){
    $stateProvider
    
    .state('home', {
        url: '/home',
        templateUrl: 'templates/home.html',
        controller: 'homeCrtl'
    })    

    $urlRouterProvider.otherwise('/home');
})