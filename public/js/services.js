angular.module('app.services', [])

    .factory('obrasService', ['$http',
        function ($http) {
            return {
                getAlgo: function(){
                    var source = 'url';
                    return $http.get(source);
                },
                newAlgo: function(parametros){
                    var source = 'url';
                    return $http.post(source+'?'+parametros);
                },
                actualizarAlgo: function(parametros){
                    var source = 'url';
                    return $http.put(source+'?'+parametros);
                }
            }
        }
    ])

    