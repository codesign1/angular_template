var promise = require('bluebird');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://localhost:5432/pipe';// Data base Access
var db = pgp(connectionString);

// Metodos Categorias 

function getAllTypes(req, res, next) {
    console.log("tipos");
  db.any('select * from tipos')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ALL types'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function createTypes(req, res, next) {
  //req.body.age = parseInt(req.body.age);
  db.none('insert into tipos(tipo, subtipo, activo)' +
      'values(${tipo}, ${subtipo}, ${activa})',
    req.query)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one Tipo'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function updateType(req, res, next) {
    db.none('update tipos set activo=$1 where tipo=$2 and subtipo=$3',
        [req.query.activa, req.query.categoria, req.query.subtipo])
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Updated tipo'
                });
        })
        .catch(function (err) {
            console.log(err);
            return next(err);
        });
}




module.exports = {
  getAllTypes: getAllTypes,
  createTypes: createTypes,
  updateType: updateType,
};