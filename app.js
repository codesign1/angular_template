/*eslint-env node*/

//------------------------------------------------------------------------------
// node.js starter application for Bluemix
//------------------------------------------------------------------------------

// This application uses express as its web server
// for more info, see: http://expressjs.com
var express = require('express');
 var path = require('path');
 var bodyParser = require('body-parser');

 var routes = require('./routes/index');


// cfenv provides access to your Cloud Foundry environment
// for more info, see: https://www.npmjs.com/package/cfenv
var cfenv = require('cfenv');

// create a new express server
var app = express();

// serve the files out of ./public as our main files
app.use(express.static(path.join(__dirname + '/public')));
// get the app environment from Cloud Foundry
var appEnv = cfenv.getAppEnv();
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json());


app.use(routes);

app.get("/auth",function(req,res){

    console.log(req.bodyParser);
    var data={name:req.body.name,
                pass:req.body.pass};
    console.log(data);
    res.render("index");
});    
// start server on the specified port and binding host
app.listen(appEnv.port, '0.0.0.0', function() {
  // print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
});



// Catch 404 and foward error handler

app.use(function(req, res, next){
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status( err.code || 500 )
    .json({
      status: 'error',
      message: err
    });
    console.log(err);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  .json({
    status: 'error',
    message: err.message
  });
});

module.exports = app;